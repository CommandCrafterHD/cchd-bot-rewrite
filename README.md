# CCHD-Bot \[Rewrite\]
This version of the CCHD-Bot (name is subject to change) is just the rewritten
version of the [CCHD-Bot](http://www.gitlab.com/CommandCrafterHD/cchd-bot) (Wich was NOT OpenSource btw.)

## Built-In commands (Mostly admin/owner/dev commands)
|**Command**                      |**Description**                                                                   |
|---------------------------------|----------------------------------------------------------------------------------|
| [help](/built-in/help.py)       | Shows you what modules are currently loaded                                      |
| [update](/built-in/update.py)   | Updates to the newest Gitlab version                                             |
| [server](/built-in/server.py)   | Lets server admins change some serverwide settings, like the prefix              |
| [eval](/built-in/eval.py)       | Lets developers/bot owners run python commands straight from discord             |
| [setgame](/built-in/set-game.py)| Lets developers/bot owners change the "playing" tag the bot shows in discord     |
| [version](/built-in/version.py) | Shows you the what version the Bot is on                                         |

## Module commands (The fun commands)
|**Command**                             |**Description**                                                                      |
|----------------------------------------|-------------------------------------------------------------------------------------|
| [me](/modules/info.py)                 | Shows some basic info about the user who entered it                |
| [math](/modules/math.py)               | Lets you solve some basic equations                                |
| [poll](/modules/poll.py)               | Lets you create polls with multiple clickable answers              |
| [random](/modules/poll.py)             | Lets you throw a die or get a random string of letters and symbols |
| [rps](/modules/rock-paper-scissors.py) | Lets you play rock-paper-scissors against the bot                  |
| [todo](/modules/todo.py)               | Lets you use a little To-Do list to manage things                  |

## How to install \[Auto\]
 + On Linux: Just run [installer.sh](/installer.sh) (Requires vim, python3.6 and pip to be installed)
 + On Windows: We dont have a windows installer ready to use, sorry :/

## How to install \[Manual\]
 + Install the requirements listed in [requirements.txt](/requirements.txt) (For Python3.6+)
 + Install [Discord Rewrite](https://github.com/Rapptz/discord.py/tree/rewrite)
 + Rename [settings-example.json](/files/settings-example.json) to "settings.json" and edit everything you need
 + Run [runbot.py](/runbot.py)
 + Enjoy!

## How to get the Bot
If you dont want to go through all the hassle to install the Bot yourself, you can just get
the version hosted by US **is currently offline, sorry :/**