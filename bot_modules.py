from typing import List, Callable, Dict

import discord
import importlib
import os
import shlex

import color


def load_module(mod_name: str):
    try:
        importlib.import_module(mod_name)
        print("[  " + color.GREEN + "OK" + color.END + "  ] Loaded module " + mod_name)
    except:
        print(
            "[ " + color.RED + color.BOLDON + "FAIL" + color.BOLDOFF + color.END + " ] Error loading module " + mod_name)


def load_package(package: str, file_suffix: str = ".py"):
    mod_dir = package.replace('.', '/')

    if os.path.isfile(mod_dir):
        load_module(package)

    for file in os.listdir(mod_dir):
        if not os.path.isfile(mod_dir + '/' + file):
            continue
        if not file.endswith(file_suffix):
            continue
        load_module(package + '.' + file[:-len(file_suffix)])


class CommandContext:
    def __init__(self,
                 discord_client: discord.Client,
                 discord_message: discord.Message,

                 prefix: str,

                 color_success: int,
                 color_failure: int,
                 color_loading: int,

                 devs: List[str],

                 std_footer: str,

                 bot_settings: Dict):
        self.discord = self.__DiscordData(discord_client, discord_message)
        self.prefix = prefix
        self.color = self.__ColorData(color_success, color_failure, color_loading)
        self.devs = devs

        self.std_footer = std_footer

        self.bot_settings = bot_settings

        self.message = discord_message.content
        self.message_parts = self.message.split(' ')
        try:
            self.args = shlex.split(self.message)
        except ValueError as e:
            if str(e) == "No closing quotation":
                self.args = shlex.split(self.message + '"')

        self.send = self.__send_send
        self.succeed = self.__send_success
        self.fail = self.__send_failure
        self.load = self.__send_loading

    async def __send_send(self, message: str) -> discord.Message:
        return await self.discord.channel.send(message)

    async def __send_success(self, title: str, description: str) -> discord.Message:
        embed = discord.Embed(title=title, description=description, color=self.color.success)
        if self.std_footer is not None:
            embed.set_footer(text=self.std_footer)
        return await self.discord.channel.send(embed=embed)

    async def __send_failure(self, description: str, title: str = "Error") -> discord.Message:
        embed = discord.Embed(title=title, description=description, color=self.color.failure)
        if self.std_footer is not None:
            embed.set_footer(text=self.std_footer)
        return await self.discord.channel.send(embed=embed)

    async def __send_loading(self, description: str = None, title: str = "Loading...") -> discord.Message:
        if description is None:
            return await self.discord.channel.send(title)
        embed = discord.Embed(title=title, description=description, color=self.color.loading)
        if self.std_footer is not None:
            embed.set_footer(text=self.std_footer)
        return await self.discord.channel.send(embed=embed)

    class __DiscordData:
        def __init__(self,
                     client: discord.Client,
                     message: discord.Message):
            self.client = client
            self.message = message
            self.channel: discord.TextChannel = message.channel
            self.author: discord.Member = message.author

    class __ColorData:
        def __init__(self,
                     success: int,
                     failure: int,
                     loading: int):
            self.success = success
            self.failure = failure
            self.loading = loading


class Command:
    def __init__(self,
                 cmd: str,
                 admin: bool,
                 dev: bool,
                 description: str,
                 syntax: List[str],
                 help: str,
                 func: Callable[[CommandContext], None]):
        self.cmd = cmd
        self.admin = admin
        self.dev = dev
        self.description = description
        self.syntax = syntax
        self.help = help

        self.func = func


commands: Dict[str, Command] = {}


def bot_command(cmd: str, description: str, syntax=None, help: str = None, admin: bool = False,
                dev: bool = False):
    if syntax is None:
        syntax = []

    def decorator(func):
        global commands
        commands[cmd.lower()] = Command(cmd, admin, dev, description, syntax, help, func)
        return func

    return decorator
