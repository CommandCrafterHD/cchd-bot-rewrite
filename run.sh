#!/usr/bin/env bash
LOG_FILE=log_$(date +%Y-%m-%d-%H-%M)
./runbot.py raw | tee ${LOG_FILE}
