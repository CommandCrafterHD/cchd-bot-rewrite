#!/usr/bin/env python3.6
import datetime
import json
import sys
from pathlib import Path
from threading import Thread

import discord
import importlib.util
import os

import color
import daemon
import server
import config

import bot_modules

# Just something that the script uses
standard_config = {
    "prefixes": {}
}

# Overall settings (to be used in the .run() calls for modules)
settings = {}

# Stuff to get loaded in from settings.json
token = ""
prefix = ""
game = ""
colors = {}
devs = []
pidfile = str(Path(os.getcwd()) / '.pid')  # '/var/run/cchdbot.pid'
logfile = str(Path(os.getcwd()) / 'latest.log')

# maintenance
debug = False
halted = False


def cat(file):
    with open(file) as handle:
        return handle.read()


def load_settings():
    try:
        # Load settings.json
        global prefix, token, colors, debug, game, devs, settings
        config = json.load(open("files/settings.json"))
        prefix = config['client']['prefix']
        token = config['client']['token']
        devs = config['devs']
        colors = config['embeds']['colors']
        debug = Path('./debug.marker').exists()
        game = config['client']['game']
        settings = {
            "prefix": prefix,
            "colors": colors,
            "footer": "Timestamp: " + str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            "game": game,
            "devs": devs
        }

        # Print loading info:
        print("Loading Settings----------------")
        print(color.YELLOW + "Prefix" + color.END + " | " + color.CYAN + prefix + color.END)
        print(color.YELLOW + "Token " + color.END + " | " + color.CYAN + token + color.END)
        print(color.YELLOW + "Debug " + color.END + " | " + color.CYAN + str(debug) + color.END)
        print(color.YELLOW + "color" + color.END + ":")
        for n, v in colors.items():
            colors[n] = v[1:]
            print("   - " + color.GREEN + n + color.END + " = " + color.CYAN + v + color.END)
        if not devs:
            print(color.YELLOW + "Admins" + color.END + " | " + color.RED + "None!" + color.END)
            print(
                color.YELLOW + "INFO: Not having any admins set is ok, but not advised. You should head over to '" + color.END + color.BOLDON + "/files/settings.json" + color.BOLDOFF + color.YELLOW + "' and add yourself!" + color.END)
        else:
            print(color.YELLOW + "Devs" + color.END + ":")
            for i in devs:
                print("   - " + color.CYAN + i + color.END)
        print("--------------------------------")
        print("")
    except Exception as e:
        print(color.RED + "ERROR: Settings could not be loaded!\nError: " + e)
        quit(1)

    # Load modules from /built-in folder
    print("Loading Built-ins---------------")
    bot_modules.load_package('built-in')
    print("--------------------------------")
    print("")

    # Load modules from /modules folder
    print("Loading Modules-----------------")
    bot_modules.load_package('modules')
    print("--------------------------------")
    print("")


class MyClient(discord.Client):
    async def on_ready(self):
        print("Bot started---------------------")
        print(color.YELLOW + "Name" + color.END + " | " + color.CYAN + client.user.name)
        print(color.YELLOW + "ID  " + color.END + " | " + color.CYAN + str(client.user.id) + color.END)
        print(color.YELLOW + "Game" + color.END + " | " + color.CYAN + game + color.END)
        print("--------------------------------")
        print("")
        await client.change_presence(game=(discord.Game(name=game)))

    async def on_message(self, message):
        # Ignore bot
        if message.author == self.user:
            return

        # --== PRIVATE MESSAGE HANDLING ==--
        if str(message.channel).startswith("Direct Message with "):
            em = discord.Embed(color=int(colors['errors'], 16), title="Error",
                               description="Writing messages in private chat\ncan mess up some commands,"
                                           "\nso please only use server chats.")
            await message.channel.send(embed=em)
            return

        # --== COMMAND HANDLING ==--
        try:
            prefix_to_use = config.load_config('core', standard_config)['prefixes'][str(message.guild.id)]
        except KeyError:
            prefix_to_use = prefix
        if message.content.startswith(prefix_to_use):
            # --== DEBUG MODE HANDLING ==--
            if debug and str(message.author.id) not in devs:
                em = discord.Embed(color=int(colors["errors"], 16), title="Error",
                                   description="Debug mode is active,\nhence all commands are disabled for now,\nplease "
                                               "be patient.")
                await message.channel.send(embed=em)
                return
            else:
                pass
            cmd = message.content.split(' ')[0][len(prefix):].lower()
            if cmd in bot_modules.commands:
                command = bot_modules.commands[cmd]
                if command.admin and not message.author.guild_permissions.administrator:
                    await message.channel.send("You need to be a server administrator to run this command!")
                    return
                if command.dev and not str(message.author.id) in devs:
                    await message.channel.send("You need to be a bot developer to run this command!")
                    return

                settings['footer'] = "Timestamp: " + str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                await command.func(bot_modules.CommandContext(
                    client,
                    message,
                    settings['prefix'],
                    int(settings['colors']['messages'], 16),
                    int(settings['colors']['errors'], 16),
                    int(settings['colors']['loading'], 16),
                    settings['devs'],
                    settings['footer'],
                    settings
                ))

            else:
                await message.channel.send("Unknown command: " + cmd)


client: MyClient


def run_raw():
    global client
    load_settings()
    client = MyClient()
    t = Thread(target=server.run, args=[])
    t.daemon = True
    t.start()
    client.run(token)


def print_help_and_exit():
    sys.stderr.write('''Use %s [start|stop|restart|log|status|raw]

start   - Start the daemon
stop    - Stop the daemon
restart - Restart the daemon
log     - View the logfile
status  - See if the daemon is online
raw     - Run the bot without a daemon. (Recommended for windows users lol)
''' % sys.argv[0])
    sys.exit(1)


class CCHDaemon(daemon.Daemon):
    def run(self):
        run_raw()


if __name__ == '__main__':
    cchd_daemon = CCHDaemon(pidfile, logfile)
    if len(sys.argv) < 2:
        print_help_and_exit()
    cmd = sys.argv[1].lower()
    if cmd == 'start':
        cchd_daemon.start()
    elif cmd == 'stop':
        cchd_daemon.stop()
    elif cmd == 'restart':
        cchd_daemon.restart()
    elif cmd == 'status':
        print(('Running with [PID=%s]' % cchd_daemon.daemon_pid) if cchd_daemon.is_running() else 'Offline')
    elif cmd == 'log':
        print(cat(logfile))
    elif cmd == 'raw':
        sys.stderr.write(('You are running in raw mode. This is discouraged. '
                          'To enable daemon functionality use %s [start|stop]\n') % sys.argv[0])
        run_raw()
    else:
        print_help_and_exit()
