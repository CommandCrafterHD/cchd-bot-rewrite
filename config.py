import json
import os

try:
    os.listdir('configs')
except FileNotFoundError:
    os.mkdir('configs')


def load_config(file_name, standard_config):
    try:
        file = json.load(open('configs/' + file_name + ".json", "r"))
        return file
    except FileNotFoundError:
        json.dump(standard_config, open('configs/' + file_name + ".json", "w+"), indent=4)
        return standard_config


def change_config(file_name, standard_config, change):
    try:
        old_config = json.load(open('configs/' + file_name + ".json", "r"))
        old_config[change[0]] = change[1]
        json.dump(old_config, open('configs/' + file_name + ".json", "w+"), indent=4)
    except FileNotFoundError:
        new_config = standard_config
        new_config[change[0]] = change[1]
        json.dump(new_config, open('configs/' + file_name + ".json", "w+"), indent=4)
