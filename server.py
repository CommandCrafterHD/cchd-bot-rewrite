import json
import traceback
import urllib.parse
from pathlib import Path

import re
import requests
import requests.exceptions
from flask import Flask, jsonify, request, redirect, url_for, render_template, session, Response
from flask_scss import Scss
from flask_session import Session
from functools import wraps

SESSION_TYPE = 'memcache'
app = Flask(__name__)
sess = Session()
config = json.load(open('files/settings.json', 'r'))

Scss(app, static_dir='static', asset_dir='assets')


class APIError(Exception):
    def __init__(self, code=500, message=None, payload=None):
        Exception.__init__(self)
        if payload is None:
            payload = {}
        self.payload = payload
        self.payload['error_code'] = code
        self.payload['message'] = message
        self.payload['success'] = False

    def to_dict(self):
        return self.payload


API_BASE_URL = 'https://discordapp.com/api/v6'


def get_discord_user(token):
    if token is None:
        return None
    resp = requests.get(API_BASE_URL + '/users/@me',
                        headers={
                            'Authorization': 'Bearer ' + token
                        })
    if 400 <= resp.status_code < 500:
        return None
    return resp.json()


def get_discord_id(token):
    user = get_discord_user(token)
    if user is not None:
        return user['id']
    return None


@app.errorhandler(APIError)
def exception_handler(error: APIError):
    error = error.to_dict()
    response = jsonify(error)
    response.status_code = error['error_code']
    return response


@app.errorhandler(Exception)
def error_handler(error: Exception):
    traceback.print_exc()
    return render_template('error.html', error_code=500)


def get_avatar_url(user_dict):
    if user_dict is None:
        return 'none.png'
    return "https://cdn.discordapp.com/avatars/" + user_dict['id'] + '/' + user_dict['avatar'] + (
        '.gif' if user_dict['avatar'][0:2] == 'a_' else '.jpg')


def is_logged_in():
    return session.get('token') is not None and get_discord_user(session.get('token')) is not None


@app.context_processor
def api_processors():
    return dict(get_avatar_url=get_avatar_url,
                user=get_discord_user(session.get('token', None)),
                is_logged_in=is_logged_in)


def get_current_discord_user():
    return get_discord_user(session.get('token', None))


class Require:
    @staticmethod
    def user(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            token = session.get('token', None)
            if token is None or get_discord_user(token) is None:
                return redirect('/login', 302)

            return func(*args, **kwargs)

        return wrapper

    @staticmethod
    def admin(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            token = request.args.get('token') or session.get('token', None)
            if token is None:
                return redirect(url_for('login'))
            if token not in config['override_tokens'] and get_discord_id(token) not in config['admins']:
                raise APIError(403)
            return func(*args, **kwargs)

        return wrapper


@app.route('/dusers/@me/image')
@Require.user
def redirect_to_discord_image():
    return redirect(get_avatar_url(get_current_discord_user()))


@app.route('/common.css')
def common_css():
    resp = Response(render_template('common.css'))
    resp.headers['Content-Type'] = 'text/css'
    return resp


@app.route('/theme.css')
def theme_css():
    theme_cookie = request.cookies.get('theme', default='default')
    if re.match('^[^a-z]+$', theme_cookie) is not None:
        raise APIError(400, message='Illegal theme')
    resp = Response(render_template(theme_cookie + '.css'))
    resp.headers['Content-Type'] = 'text/css'
    return resp


@app.route('/logout')
def logout():
    session['token'] = None
    return redirect('/')


@app.route('/set-theme')
def set_cookie():
    return_url = request.args.get('return', '/')
    new_theme = request.args.get('theme', None)
    resp = redirect(return_url, 302)
    if new_theme is not None:
        resp.set_cookie('theme', new_theme)
    return resp


@app.route('/configs', methods=['GET'])
@Require.admin
def read_config():
    return jsonify(config)


@app.route('/')
def index():
    return render_template('index.html')


def is_debug():
    return Path('./debug.marker').exists() or config.get('debug', False)


def current_server():
    return 'localhost' if is_debug() else 'commandcrafterhd.de'


def create_auth_url():
    return 'https://discordapp.com/api/oauth2/authorize?redirect_uri=%s&scope=identify&response_type=code' \
           '&client_id=%s' % (
               urllib.parse.quote_plus(f'https://{current_server()}:5000/auth'), str(config['client_id']))


def exchange_code(code):
    data = {
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': f'https://{current_server()}:5000/auth'
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post('https://discordapp.com/api/oauth2/token', data, headers)
    r.raise_for_status()
    return r.json()['access_token']


@app.route('/login')
def login():
    return redirect(create_auth_url())


@app.route('/auth')
def auth():
    token = exchange_code(request.args.get('code'))
    session['token'] = token
    return redirect('/')


@app.route('/configs', methods=['POST', 'PUT'])
@Require.admin
def write_config():
    return jsonify({
        'success': True
    })


@app.route('/user')
@Require.user
def user():
    return render_template('user.html', user=get_discord_user(session.get('token')))


def run():
    app.secret_key = config['app_config']['session_secret']
    app.config.update(config['app_config'])
    sess.init_app(app)
    app.run(debug=is_debug(),
            host=('127.0.0.1' if is_debug() else '0.0.0.0'),
            ssl_context=('files/cert.crt', 'files/cert.key'))


if __name__ == '__main__':
    run()
