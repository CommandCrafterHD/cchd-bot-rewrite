import discord
import random
import bot_modules


@bot_modules.bot_command("random", "Rolls a dice or generates a random string", ["dice", "string [<length>]"],
                         """
The **dice** sub-command generates a random number from 1 to 6 (like a dice throw)
The **string** sub-command generates a random string of the specified length (8 by default)""")
async def random_cmd(ctx: bot_modules.CommandContext):
    args = ctx.message_parts
    if len(args) <= 1:
        em = discord.Embed(color=ctx.color.success, title="Random - Info")
        em.add_field(name="Sub-command", value="**dice**\n\n\n**string**\n\n")
        em.add_field(name="Meaning / Usage",
                     value="The **dice** sub-command lets\nyou throw a die with 6 Sides.\n\nThe **string** "
                           "sub-command lets you\ngenerate a random set of characters\nif no length value is "
                           "given, 8 is the default.\nExample: **{}random string 16**\nwould give you a string "
                           "with a lengt of 16".format(ctx.prefix))
        em.set_footer(text=ctx.std_footer)
        await ctx.discord.channel.send(embed=em)
    else:
        if args[1] == "dice":
            em = discord.Embed(color=ctx.color.success, title="Random: Dice",
                               description="You rolled a " + random.choice(
                                   [":one:", ":two:", ":three:", ":four:", ":five:", ":six:"]) + "!")
            em.set_thumbnail(url=ctx.discord.client.user.avatar_url)
            em.set_footer(text=ctx.std_footer)
            await ctx.discord.channel.send(embed=em)
        elif args[1] == "string":
            pool = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
                    "T", "U", "V", "W", "X", "Y", "Z",
                    "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "?", "-", "."]
            string = ""
            length = 8
            if len(args) > 2:
                if args[2] == "final_countdown":
                    await ctx.send("https://youtu.be/9jK-NcRmVcw")
                try:
                    length = int(args[2])
                except ValueError:
                    await ctx.fail("**" + args[2] + "** is not a valid length! (Has to be a full number)")
                    return
            if length < 0:
                await ctx.fail("Really? Less than zero chars?")
                return
            if length > 2019:
                await ctx.fail("Too many letters requested. %d > 2019" % length)
                return
            while length > 0:
                pick = random.choice(pool)
                if random.randrange(0, 2) == 1:
                    try:
                        pick = pick.lower()
                    except:
                        pass
                string += pick
                length -= 1
            await ctx.succeed("Random: String", "Your random string is:\n" + string)
