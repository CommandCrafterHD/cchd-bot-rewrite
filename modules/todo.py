import bot_modules
import config

standard_config = {}
core_standard_config = {
    "prefixes": {}
}


@bot_modules.bot_command('todo', 'Lets you use a little To-Do list to manage things', ['add <TO-DO Item>', 'check <Item index>', 'remove <Item index>', 'list'])
async def todo_command(ctx: bot_modules.CommandContext):
    prefixes = config.load_config('core', standard_config)['prefixes']
    prefix = ctx.prefix
    if str(ctx.discord.channel.guild.id) in prefixes:
        prefix = prefixes[str(ctx.discord.channel.guild.id)]
    args = ctx.message_parts
    if len(args) > 1:
        if args[1] == 'list':
            todo_lists = config.load_config('todo', standard_config)
            todo_list = None
            if str(ctx.discord.message.author.id) in todo_lists:
                todo_list = todo_lists[str(ctx.discord.message.author.id)]
            if todo_list == None or todo_list == []:
                await ctx.succeed(title="To-Do", description="Your To-Do list seems to be empty!\n"
                                                             "How about we change that? Try:\n"
                                                             "`{}todo add This is an example!`\n"
                                                             "And it should add a list item with the name:\n"
                                                             "`This is an example!` its easy, isnt it?".format(prefix))
            else:
                index = 1
                todos = ""
                for i in todo_list:
                    if i[1]:
                        todos += str(index) + ": " + str(i[0]) + " :white_check_mark:\n"
                    else:
                        todos += str(index) + ": " + str(i[0]) + "\n"
                    index += 1
                await ctx.succeed(title="To-Do", description=todos)
        elif args[1] == 'add':
            if len(args) > 2:
                length_to_remove = len(ctx.prefix) + 9
                if len(ctx.message[length_to_remove:]) >= 100:
                    await ctx.fail("Im sorry, but try to keep it a bit shorter! The maximum length\n"
                                   "for a single To-Do item is 100 characters!")
                    return
                todos = []
                if str(ctx.discord.message.author.id) in config.load_config('todo', standard_config):
                    todos = config.load_config('todo', standard_config)[str(ctx.discord.message.author.id)]
                if len(todos) < 10:
                    todos.append((ctx.message[length_to_remove:], False))
                    config.change_config('todo', standard_config, (str(ctx.discord.message.author.id), todos))
                    await ctx.succeed(title="To-Do", description="This added the item: `" + ctx.message[length_to_remove:] + '`')
                else:
                    await ctx.fail("Whoops! Seems like your To-Do list is already 10 items long!\n"
                                   "Im sorry to tell you but because of filesizes every user can only have 10 entries\n"
                                   "You can remove some, for example entry 1 (`{}`) can be removed using:\n"
                                   "`{}todo remove 1`".format(str(todos[0]), prefix))
                    return
            else:
                await ctx.fail("You need to give the To-Do item a name you silly!")
        elif args[1] == 'check':
            if len(args) > 2:
                try:
                    item_to_check = int(args[2])
                except ValueError:
                    await ctx.fail("You need to specify a number!")
                    return
                todos = []
                if str(ctx.discord.message.author.id) in config.load_config('todo', standard_config):
                    todos = config.load_config('todo', standard_config)[str(ctx.discord.message.author.id)]
                if todos == []:
                    await ctx.succeed("You dont even have items to check off!")
                else:
                    todos[item_to_check - 1][1] = True
                    await ctx.succeed(title="To-Do", description="Checked off the Item with number: " + str(item_to_check) + " (`{}`)".format(todos[item_to_check - 1][0]))
                    config.change_config('todo', standard_config, (str(ctx.discord.message.author.id), todos))
            else:
                await ctx.fail("You need to specify an item index to check off! For a list of items on your list, use: `" + prefix + "todo list`")
        elif args[1] == 'remove':
            if len(args) > 2:
                try:
                    item_to_check = int(args[2])
                except ValueError:
                    if args[2] == "*":
                        item_to_check = "*"
                    else:
                        await ctx.fail("You need to specify a number!")
                        return
                todos = []
                if str(ctx.discord.message.author.id) in config.load_config('todo', standard_config):
                    todos = config.load_config('todo', standard_config)[str(ctx.discord.message.author.id)]
                if todos == []:
                    await ctx.fail("You dont even have anything to remove!")
                    return
                else:
                    if item_to_check == "*":
                        await ctx.succeed(title="To-Do", description="Removed all the items of your To-Do list!")
                        todos = []
                        config.change_config('todo', standard_config, (str(ctx.discord.message.author.id), todos))
                    else:
                        await ctx.succeed(title="To-Do", description="Removed the Item with number: " + str(item_to_check) + " (`{}`)".format(todos[item_to_check - 1][0]))
                        todos.pop(item_to_check - 1)
                        config.change_config('todo', standard_config, (str(ctx.discord.message.author.id), todos))
            else:
                await ctx.fail("You need to specify an item index to remove! For a list of items on your list, use: `" + prefix + "todo list`")
        else:
            await ctx.fail("`" + str(args[1]) + "` is not a valid option!")
    else:
        await ctx.fail("Too few arguments!")
