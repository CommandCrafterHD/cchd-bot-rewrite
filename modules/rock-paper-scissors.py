import discord
import random

import bot_modules


def replace_emojis_rps(input):
    if input == "rock":
        return ":fist:"
    if input == "paper":
        return ":raised_hand:"
    if input == "scissors":
        return ":v:"


@bot_modules.bot_command("rps", "Lets you play rock paper scissors against the bot.", ["(rock|paper|scissors)"])
async def rps_cmd(ctx: bot_modules.CommandContext):
    args = ctx.message_parts
    if len(args) <= 1:
        await ctx.fail("Yout need to specify which one to picx:\n"
                       "rock\n"
                       "paper\n"
                       "scissors\n"
                       "For example: **" + ctx.prefix + "rps rock**")
    else:
        if args[1] == "rock" or args[1] == "paper" or args[1] == "scissors":
            state = 0
            bot_choice = random.choice(["rock", "paper", "scissors"])
            if args[1] == "rock":
                if bot_choice == "scissors":
                    state = 1
                elif bot_choice == "rock":
                    state = 2
            elif args[1] == "paper":
                if bot_choice == "rock":
                    state = 1
                elif bot_choice == "paper":
                    state = 2
            elif args[1] == "scissors":
                if bot_choice == "paper":
                    state = 1
                elif bot_choice == "scissors":
                    state = 2
            if state == 1:
                await ctx.succeed("Rock, Paper, Scissors!",
                                  "You WON!\n"
                                  "You picked: " + replace_emojis_rps(args[1]) +
                                  "\n:robot: picked: " + replace_emojis_rps(bot_choice))
            elif state == 0:
                await ctx.succeed("Rock, Paper, Scissors!",
                                  "You LOST!\n"
                                  ":robot: picked: " + replace_emojis_rps(bot_choice) + "\n"
                                  "You picked: " + replace_emojis_rps(
                                       args[1]))
            elif state == 2:
                await ctx.succeed("Rock, Paper, Scissors!",
                                  "You DRAWED!\n"
                                  ":robot: picked: " + replace_emojis_rps(bot_choice) + "\n"
                                  "You picked: " + replace_emojis_rps(args[1]))
        else:
            await ctx.send(args[1] + "does not count as a choice")
