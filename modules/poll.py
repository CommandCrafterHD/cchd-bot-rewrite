import bot_modules


@bot_modules.bot_command("poll", "Creates a poll", ["option1 option2 ... optionN"],
                         """Additional information:
To create a title for the poll end the first argument with a colon or question mark (title: / title?)
You can use whitespaces in title/options, by surrounding them with double quotes ("some text")
If you want to use quotes in your text, you have to put a backslash in front (\\\\")
If you want to use backslashes in your text, you have to put another one in front (\\\\\\\\)""")
async def poll_cmd(ctx: bot_modules.CommandContext):
    options = ctx.args
    if len(options) == 1:
        await ctx.fail("Needs options to choose from!")
        return

    options = options[1:]

    if len(options) > 20:
        await ctx.fail("Can't have more than 20 options. :(")
        return

    msg = ctx.discord.author.mention + " has started a poll:"

    if options[0][-1] in [':', '?']:
        if len(options) == 1:
            await ctx.fail("Needs options to choose from!")
            return
        msg += '\n' + options[0]
        options = options[1:]

    for i in range(len(options)):
        msg += "\n" + chr(i + 65) + ": " + options[i]
    sent_msg = await ctx.send(msg)

    for i in range(len(options)):
        await sent_msg.add_reaction(chr(0x1F1E6 + i))
