import discord

import bot_modules


@bot_modules.bot_command('me',
                         description='Shows general information about you')
async def me_command(ctx: bot_modules.CommandContext):
    member: discord.Member = ctx.discord.author
    embed = discord.Embed(title='Info about %s' % member.display_name)

    embed.set_thumbnail(url=member.avatar_url)
    embed.colour = member.color

    embed.add_field(name='Name', value=member.name, inline=True)
    embed.add_field(name='Discriminator', value=member.discriminator, inline=True)
    embed.add_field(name='ID / Snowflake', value=str(member.id), inline=True)
    embed.add_field(name='Server Join date', value=str(member.joined_at).split('.')[0], inline=True)
    embed.add_field(name='Discord member since', value=str(member.created_at).split('.')[0], inline=True)

    if member.game:
        game: discord.Game = member.game
        if game.url:
            embed.add_field(name='Game', value='[%s](%s)' % (game.name, game.url), inline=True)
        else:
            embed.add_field(name='Game', value=game.name, inline=True)

    await ctx.discord.channel.send(embed=embed)


