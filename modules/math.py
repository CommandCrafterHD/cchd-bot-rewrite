import bot_modules
import re


def quick_and_dirty_math_eval(code):
    code = re.sub(r"[^0-9+\-/*.]", "", string=str(code))
    try:
        return eval(code)
    except ZeroDivisionError:
        return "ZeroDivision"
    except SyntaxError:
        return "Invalid Expression"
    except Exception as e:
        return "Unknown Error. Please Contact an Admin with the error message: " + str(e)


@bot_modules.bot_command("math", "Lets you solve simple equations", ["<equation>"])
async def math_command(ctx: bot_modules.CommandContext):
    args = ctx.message_parts
    if len(args) <= 1:
        await ctx.fail("Too few arguments!")
    else:
        if args[1] == '2+2-1':
            await ctx.succeed(title="2+2-1=", description="Quick maths :3")
            return
        returned = quick_and_dirty_math_eval(args[1])
        if str(returned) == "42":
            await ctx.succeed(title="Its always 42 isn't it?", description="The answer to the universe and everything: 42")
            return
        if returned == "ZeroDivision":
            await ctx.fail("Division by 0 not possible")
            return
        if str(returned).startswith("Unknown Error."):
            await ctx.fail(str(returned))
            return
        if returned == "Invalid Expression":
            await ctx.fail("Invalid Expression")
            return
        else:
            await ctx.succeed(title=str(args[1]) + "=", description=str(returned))
            return
