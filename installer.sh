#!/usr/bin/env bash
git clone https://gitlab.com/CommandCrafterHD/cchd-bot-rewrite.git
cd cchd-bot-rewrite
pip3 install -r requirements.txt
cp files/settings-example.json files/settings.json
vi files/settings.json
./run.sh
