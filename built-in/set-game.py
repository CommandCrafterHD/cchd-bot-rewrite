import discord

import bot_modules


@bot_modules.bot_command("setGame", "Sets the \"playing\" tag!", ["<game>"], dev=True)
async def set_game_cmd(ctx: bot_modules.CommandContext):
    args =ctx.message_parts
    if len(args) <= 1:
        await ctx.discord.client.change_presence(game=(discord.Game(name=ctx.bot_settings['game'])))
        await ctx.send("Game reset to: `" + ctx.bot_settings['game'] + "`")
    else:
        game = ""
        for i in args[1:]:
            game += str(i + " ")
        await ctx.discord.client.change_presence(game=(discord.Game(name=game)))
        await ctx.send("Game set to: `" + game + "`")
