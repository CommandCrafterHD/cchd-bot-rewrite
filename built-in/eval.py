import ast
import shlex

import bot_modules


@bot_modules.bot_command("eval", """Evals a certain python expression.""", ["<code>"], dev=True)
async def eval_command(ctx: bot_modules.CommandContext):
    if len(ctx.args) < 2:
        await ctx.fail(description="Apparently being an admin doesn't mean you aren't a total idiot. You have to "
                                   "give me an expression to eval you moron",
                       title="Eval failed.")
        return
    elif len(ctx.args) > 2:
        await ctx.fail(description="Please consider quoting your input. For example %s" %
                                   shlex.quote(' '.join(ctx.message_parts[1:])))
    else:
        try:
            block = ast.parse(ctx.args[1], mode='exec')
            last = ast.Expression(block.body.pop().value)
        except KeyboardInterrupt:
            raise
        except SystemExit:
            raise
        except BaseException as e:
            await ctx.fail("Compilation failed: %r" % e)
            return

        _globals, _locals = {}, {
            'message': ctx.discord.message,
            'author': ctx.discord.author,
            'channel': ctx.discord.channel,
            'client': ctx.discord.client
        }
        try:
            exec(compile(block, '<string>', mode='exec'), _globals, _locals)
        except KeyboardInterrupt:
            raise
        except SystemExit:
            raise
        except BaseException as e:
            await ctx.fail("Evaluation failed: %r" % str(e))
            return

        try:
            compiled = compile(last, '<string>', mode='eval')
        except KeyboardInterrupt:
            raise
        except SystemExit:
            raise
        except BaseException as e:
            await ctx.fail("Last statement has to be an expression: %r" % str(e))
            return

        try:
            result = eval(compiled, _globals, _locals)
        except KeyboardInterrupt:
            raise
        except SystemExit:
            raise
        except BaseException as e:
            await ctx.fail("Evaluation failed: %r" % str(e))
            return

        await ctx.succeed(title="Evaluated successfully", description="""```py
%s```""" % str(result))
