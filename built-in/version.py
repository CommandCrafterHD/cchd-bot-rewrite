import os
from git.exc import GitError
from git.repo import Repo

import bot_modules


@bot_modules.bot_command(cmd="version",
                         description="Displays the current version",
                         dev=True)
async def version_command(ctx: bot_modules.CommandContext):
    try:
        local_repo = Repo(os.getcwd())
    except GitError:
        await ctx.succeed(title="Version: RELEASE",
                          description="No git repository found.")
        return

    hexsha = local_repo.head.commit.hexsha
    commit_message = local_repo.head.commit.message
    commiter = local_repo.head.commit.author
    branch = local_repo.active_branch.name
    await ctx.succeed(title="Branch: " + branch, description="Latest commit:\n#%s\n%s\nby %s"
                                                             % (hexsha, commit_message, commiter))
