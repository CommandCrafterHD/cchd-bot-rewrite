import bot_modules
import os


@bot_modules.bot_command('update', "Updates the bot to the newest Gitlab version", [''], dev=True)
async def update_command(ctx: bot_modules.CommandContext):
    if os.name is not "nt":
        try:
            os.system("./update.sh")
        except Exception as e:
            await ctx.fail("An error has occoured:" + str(e))
    else:
        await ctx.fail("Cant run update.sh since this bot instance is running on windows")
