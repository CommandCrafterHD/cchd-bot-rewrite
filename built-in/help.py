import discord

import bot_modules
import config


def help_string(command, prefix):
    cmd_string = prefix + command.cmd
    command_string = command.description
    if command.syntax:
        command_string += "\n\nUsage:"
        for usage in command.syntax:
            command_string += "\n" + cmd_string + " " + usage

    return command_string


@bot_modules.bot_command("help", "Displays a list of all available commands", ["", "<command>"])
async def help_cmd(ctx: bot_modules.CommandContext):
    args = ctx.message_parts
    standard_config = {
        "prefixes": {}
    }
    prefixes = config.load_config('core', standard_config)['prefixes']
    prefix = ctx.prefix
    if str(ctx.discord.channel.guild.id) in prefixes:
        prefix = prefixes[str(ctx.discord.channel.guild.id)]
    if len(args) > 1:
        for i in range(len(args)-1):
            cmd = args[i+1].lower()
            if cmd not in bot_modules.commands:
                await ctx.fail(title="Command not found",
                               description="The command \"%s\" could not be found!" % cmd)
                continue

            command = bot_modules.commands[cmd]
            help_str = help_string(command, prefix)
            if command.help is not None:
                cmd_help = command.help
                if cmd_help.startswith('\n'):
                    cmd_help = cmd_help[1:]
                help_str += "\n\n" + cmd_help
            await ctx.succeed(command.cmd, help_str)
        return

    commands = discord.Embed(color=ctx.color.success, title="Commands available:")
    show_dev_commands = False
    show_admin_commands = False
    dev_commands = discord.Embed(color=discord.Colour.orange(), title="Dev commands available:")
    admin_commands = discord.Embed(color=discord.Colour.orange(), title="Admin commands available:")
    count_commands = 0
    count_admin_commands = 0
    count_dev_commands = 0

    if str(ctx.discord.author.id) in ctx.devs:
        show_dev_commands = True
    if ctx.discord.author.guild_permissions.administrator:
        show_admin_commands = True

    for _, command in bot_modules.commands.items():
        cmd_str = prefix + command.cmd
        command_string = help_string(command, prefix)

        if not command.admin and not command.dev:
            commands.add_field(name=cmd_str, value=command_string, inline=False)
            count_commands += 1
        if command.admin:
            admin_commands.add_field(name=cmd_str, value=command_string, inline=False)
            count_admin_commands += 1
        if command.dev:
            dev_commands.add_field(name=cmd_str, value=command_string, inline=False)
            count_dev_commands += 1
    commands.set_footer(text=ctx.std_footer)
    dev_commands.set_footer(text=ctx.std_footer)
    admin_commands.set_footer(text=ctx.std_footer)
    if count_commands > 0:
        await ctx.discord.channel.send(embed=commands)
    if show_admin_commands and count_admin_commands > 0:
        await ctx.discord.channel.send(embed=admin_commands)
    if show_dev_commands and count_dev_commands > 0:
        await ctx.discord.channel.send(embed=dev_commands)
