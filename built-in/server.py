import bot_modules
import config

standard_config = {
    "prefixes": {}
}


@bot_modules.bot_command("server", "Lets server admins change some basic settings for their servers", ["prefix <new prefix>"], admin=True)
async def server_command(ctx: bot_modules.CommandContext):
    args = ctx.message_parts
    if len(args) <= 1:
        await ctx.fail("Too few arguments!")
    else:
        if args[1] == "prefix":
            prefixes = config.load_config('core', standard_config)['prefixes']
            prefixes[str(ctx.discord.message.guild.id)] = args[2]
            config.change_config('core', standard_config, ('prefixes', prefixes))
            await ctx.succeed(title="Prefix changed!", description="Changed to: **" + str(args[2]) + "**")
        else:
            await ctx.fail(str(args[1]) + " is not a valid option to set!")
